/*
 * indication.c
 *
 *  Created on: Jan 18, 2022
 *      Author: KARMA
 */

#include "indication.h"
#include "beeper.h"
#include "stm32f1xx_hal.h"


#define _INDICATION_ITR_QTY		(2)


typedef enum {
	LED_STATE_OFF,
	LED_STATE_RED,
	LED_STATE_BLUE
}led_state_t;

typedef struct {
	indication_t IndType;
	led_state_t  LedState[_INDICATION_ITR_QTY];
	uint16_t 	 TickDelay[_INDICATION_ITR_QTY];
}indication_param_t;

static indication_param_t _indication_param[INDICATION_TYPES_QTY] = {
	{
		.IndType = INDICATION_OFF,
		.LedState 	= {LED_STATE_OFF, LED_STATE_OFF},
		.TickDelay 	= {10, 10}
	},
	{
		.IndType = INDICATION_HOLD,
		.LedState 	= {LED_STATE_RED, LED_STATE_BLUE},
		.TickDelay 	= {700, 100}
	},
	{
		.IndType = INDICATION_STARTING,
		.LedState 	= {LED_STATE_BLUE, LED_STATE_OFF},
		.TickDelay 	= {80, 300}
	},
	{
		.IndType = INDICATION_ACTIVE,
		.LedState 	= {LED_STATE_BLUE, LED_STATE_OFF},
		.TickDelay 	= {20, 100}
	}
};


static void _SetLedState(led_state_t state)
{
	switch(state)
	{
		case LED_STATE_OFF:
			GPIOC->BSRR = GPIO_PIN_13;
			GPIOA->BSRR = GPIO_PIN_9;
			GPIOA->CRH |= 0x01<<GPIO_CRH_CNF9_Pos;
			break;
		case LED_STATE_RED:
			GPIOC->BSRR = GPIO_PIN_13;
			GPIOA->BSRR = GPIO_PIN_9;
			GPIOA->CRH &= ~(0x02<<GPIO_CRH_CNF9_Pos | 0x01<<GPIO_CRH_CNF9_Pos);
			break;
		case LED_STATE_BLUE:
			GPIOC->BRR = GPIO_PIN_13;
			GPIOA->BRR = GPIO_PIN_9;
			GPIOA->CRH &= ~(0x02<<GPIO_CRH_CNF9_Pos | 0x01<<GPIO_CRH_CNF9_Pos);
			break;
		default:
			break;
	}
}

void INDICATION_Init(void)
{
	_SetLedState(LED_STATE_OFF);
	BEEPER_Beep(3500);
	HAL_Delay(100);
	BEEPER_Off();
}

void INDICATION_InitEvent(void)
{
	for(uint8_t i = 0; i < 3; i++)
	{
		_SetLedState(LED_STATE_RED);
		BEEPER_Beep(3500);
		HAL_Delay(50);

		_SetLedState(LED_STATE_BLUE);
		BEEPER_Off();
		HAL_Delay(50);
	}
	_SetLedState(LED_STATE_OFF);
	HAL_Delay(300);
}

void INDICATION_DeinitEvent(void)
{
	for(uint8_t i = 0; i < 4; i++)
	{
		_SetLedState(LED_STATE_RED);
		BEEPER_Beep(3500 - (i * 200));
		HAL_Delay(80);

		_SetLedState(LED_STATE_BLUE);
		BEEPER_Off();
		HAL_Delay(80);
	}
	_SetLedState(LED_STATE_OFF);
	HAL_Delay(300);
}

void INDICATION_ActivateEvent(void)
{
	for(uint8_t i = 0; i < 4; i++)
	{
		_SetLedState(LED_STATE_BLUE);
		BEEPER_Beep(4000 + (i * 200));
		HAL_Delay(50);

		_SetLedState(LED_STATE_OFF);
		BEEPER_Off();
		HAL_Delay(50);
	}
}

void INDICATION_Idle(indication_t indType)
{
	static uint8_t  _itrNum = 0;
	static uint32_t _nextItrTick = 0;

	if(_nextItrTick < HAL_GetTick())
	{
		uint8_t indTypeInd;

		for(indTypeInd = 0; indTypeInd < INDICATION_TYPES_QTY; indTypeInd++)
		{
			if(_indication_param[indTypeInd].IndType == indType) {break;}
		}

		_nextItrTick = HAL_GetTick() + _indication_param[indTypeInd].TickDelay[_itrNum];
		_SetLedState(_indication_param[indTypeInd].LedState[_itrNum]);
		_itrNum = (++_itrNum < _INDICATION_ITR_QTY) ? _itrNum : 0;
	}
}

