/*
 * event_manager.c
 *
 *  Created on: Jan 18, 2022
 *      Author: KARMA
 */

#include "event_manager.h"
#include "usbd_customhid.h"

#define _EVENT_CHECK_ACTIVE_INTERVAL	(2000)
#define _EVENT_CHECK_ACTIVE_ITR_QTY		(4)
#define _EVENT_CHECK_RETRY_QTY			(2)

#define _EVENT_CHECK_KEY_CAPS_LOCK		(0x39)
#define _EVENT_CHECK_KEY_NUM_LOCK		(0x53)
#define _EVENT_CHECK_KEY_SCROLL_LOCK	(0x47)


static event_code_t _eventCode = EVENT_CODE_NONE;
static uint32_t 	_eventId = 0;

extern USBD_HandleTypeDef hUsbDeviceFS;


void EVENT_MANAGER_AddCode(event_code_t code)
{
	_eventCode |= code;
}

bool EVENT_MANAGER_CheckCode(event_code_t code)
{
	bool res = (code & _eventCode) == code;
	_eventCode &= ~code;
	return res;
}

void EVENT_MANAGER_ResetAllCodes(void)
{
	_eventCode = 0;;
}

void EVENT_MANAGER_SetEventId(uint8_t id)
{
	_eventId = id;
}

uint8_t EVENT_MANAGER_GetEventId(void)
{
	uint8_t res = _eventId;
	_eventId = 0;
	return res;
}


bool EVENT_MANAGER_CheckEventIdChange(uint32_t eventIdMask)
{
	static uint32_t _oldEventId = 0;
	bool res = ((_eventId ^ _oldEventId) & eventIdMask) == eventIdMask;
	_oldEventId = _eventId;
	return res;
}

bool EVENT_MANAGER_CheckActive(void)
{
	static bool 	_isActive = false;
	static uint8_t  _itrNum = 0;
	static uint8_t  _retryNum = 0;
	static uint32_t _nextItrTick = 0;

	if(_nextItrTick < HAL_GetTick())
	{
		uint8_t  data[8] = {0};
		uint16_t tickInterval = 0;

		switch(_itrNum)
		{
		case 0:
			data[2] = _EVENT_CHECK_KEY_NUM_LOCK;
			tickInterval = 30;
			break;

		case 1:
			data[2] = 0x00;
			tickInterval = 50;
			break;

		case 2:
			data[2] = _EVENT_CHECK_KEY_NUM_LOCK;
			tickInterval = 30;
			break;

		case 3:
			if(EVENT_MANAGER_CheckCode(EVENT_CODE_ACTIVE))
			{
				_isActive = true;
				_retryNum = 0;
			}
			else if(_isActive)
			{
				if(_retryNum < _EVENT_CHECK_RETRY_QTY)
				{
					_retryNum++;
				}
				else
				{
					_retryNum = 0;
					_isActive = false;
				}
			}
			data[2] = 0x00;
			tickInterval = _isActive ? _EVENT_CHECK_ACTIVE_INTERVAL : 50;
			break;
		}

		_nextItrTick = HAL_GetTick() + tickInterval;
		USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, data, 8);
		_itrNum = (++_itrNum < _EVENT_CHECK_ACTIVE_ITR_QTY) ? _itrNum : 0;
	}

	return _isActive;
}


