# Параметры проекта, общие для всех конфигураций сборки

# Выбор компиляции
GCC     := arm-none-eabi-gcc
G++     := arm-none-eabi-g++
SIZE    := arm-none-eabi-size
OBJDUMP := arm-none-eabi-objdump
OBJCOPY := arm-none-eabi-objcopy

# Название проекта
PRJ_NAME := usb_tester

# Расширение исполняемого файла (".elf", ".exe", "" и др.)
PRJ_OUT_EXTENSION := .elf

# Флаги компиляции
PRJ_CFLAGS := \
-O1 \
-mcpu=cortex-m3 \
-mfloat-abi=soft \
-mthumb \
-Wno-unused-parameter \
-Wno-redundant-decls \
-Wno-old-style-declaration \

# Флаги линкера
PRJ_LFLAGS := \
-T STM32F103C8TX_FLASH.ld \
--specs=nano.specs \
--specs=nosys.specs \
-Wl,--gc-sections -ffreestanding \

# Дефайны
PRJ_DEFINES := \
STM32F103xB \
USE_HAL_DRIVER \
DEBUG \

# Пути к заголовочным файлам
PRJ_INC_PATHS := \
USB_DEVICE/Target \
Drivers/CMSIS/Device/ST/STM32F1xx/Include \
Middlewares/ST/STM32_USB_Device_Library/Class/CustomHID/Inc \
Drivers/CMSIS/Include \
Core/Inc \
USB_DEVICE/App \
Drivers/STM32F1xx_HAL_Driver/Inc/Legacy \
Drivers/STM32F1xx_HAL_Driver/Inc \
Middlewares/ST/STM32_USB_Device_Library/Core/Inc \
Core/Modules \

# Пути к файлам с исходным кодом
PRJ_SRC_PATHS := \
Core \
Drivers \
Middlewares \
USB_DEVICE \

# Пути к скомпилированным библиотекам
PRJ_LIB_PATHS := \

# Наименования библиотек
PRJ_LIBRARIES := \

PRJ_GCC_DEF := \

# Прочие файлы, при изменении которых необходимо перезапускать сборку
PRJ_OTHER_DEPS := \
STM32F103C8TX_FLASH.ld \
