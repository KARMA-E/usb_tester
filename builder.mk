include global.mk
include project_configs/build_configs/$(BC_NAME).mk
include project_configs/common.mk

#----------------------------------------------------------------------------------------------------------------------

# Путь к артефактам текущей конфигурации сборки
OUTPUT_PATH := $(G_OUTPUT_PATH)/$(BC_NAME)/

# Наименования артефактов текущей конфигурации сборки
OUT := $(OUTPUT_PATH)$(PRJ_NAME)_$(BC_NAME)$(PRJ_OUT_EXTENSION)
MAP := $(OUTPUT_PATH)$(PRJ_NAME)_$(BC_NAME).map
LSS := $(OUTPUT_PATH)$(PRJ_NAME)_$(BC_NAME).lss
BIN := $(OUTPUT_PATH)$(PRJ_NAME)_$(BC_NAME).bin

# Функция генерации списка файлов с рекурсивным поиском по переданому 
# списку путей $1 и фильтрацией по регулярному выражению $2
rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

# Пути к файлам с исходным кодом
SRC_PATHS := $(PRJ_SRC_PATHS) $(BC_SRC_PATHS)

# Списки файлов с исходным кодом (c++, c, asm)
CPP_SRC := $(call rwildcard, $(SRC_PATHS), *.cpp)
C_SRC   := $(call rwildcard, $(SRC_PATHS), *.c)
S_SRC   := $(call rwildcard, $(SRC_PATHS), *.s)

# Списки объектных файлов (c++, c, asm)
CPP_OBJ := $(addprefix $(OUTPUT_PATH), $(CPP_SRC:.cpp=.o))
C_OBJ   := $(addprefix $(OUTPUT_PATH), $(C_SRC:.c=.o))
S_OBJ   := $(addprefix $(OUTPUT_PATH), $(S_SRC:.s=.o))

# Список всех объектных файлов
ALL_OBJ := $(CPP_OBJ) $(C_OBJ) $(S_OBJ)

# Список файлов с информацией о зависимостях исходных и заголовочных файлов
DEPENDS := $(ALL_OBJ:.o=.d)

# Подключаемые библиотеки
LIBS := $(addprefix -l, $(PRJ_LIBRARIES) $(BC_LIBRARIES))

# Список файлов при изменении которых необходимо перезапускать сборку всего проекта
OTHER_DEPS := \
$(PRJ_OTHER_DEPS) $(PRJ_OTHER_DEPS) \
project_configs/common.mk \
project_configs/build_configs/$(BC_NAME).mk \
builder.mk \
global.mk \
makefile \

#----------------------------------------------------------------------------------------------------------------------

# Флаги компиляции С файлов
GCC_CFLAGS := \
$(addprefix -D, $(PRJ_DEFINES) $(BC_DEFINES)) \
$(addprefix -I, $(PRJ_INC_PATHS) $(BC_INC_PATHS)) \
$(addprefix -L, $(PRJ_LIB_PATHS) $(BC_LIB_PATHS)) \
-MMD -MP \
-ffunction-sections -fdata-sections \
-g -fms-extensions -static -static-libgcc -static-libstdc++ \
-Wall -Wextra -Wshadow -Wredundant-decls -Wno-unused-result \
$(PRJ_CFLAGS) $(BC_CFLAGS) \
-pipe $(PRJ_GCC_DEF) \

# Флаги компиляции С++ файлов
G++_CFLAGS := \
-std=c++17 \
$(GCC_CFLAGS) \

# Флаги линкера
G++_LFLAGS := \
-ggdb -Wl,-Map=$(MAP) \
$(PRJ_LFLAGS) $(BC_LFLAGS) \

#----------------------------------------------------------------------------------------------------------------------

# Файлы, необходимые в VS Code для подсветки кода (содержит дефайны и флаги компилятора для последней скомпилированной конфигурации)
ifeq ($(IDE),vscode)
VS_CODE_FILES := .vscode/compile_commands.json
endif

#----------------------------------------------------------------------------------------------------------------------

# Главная цель сборки, реквизиты которой - набор главных артефактов сборки
all: $(OUT) $(BIN) $(LSS) $(VS_CODE_FILES)
	@echo "Building done: $(BC_NAME)"

# Цель для сборки файлов, необходимых в VS Code (не выполняется, если VS_CODE_FILES пусто)
$(VS_CODE_FILES) : $(OUT)
# Т.к. в режиме -n выполнение цели all не достигается, то и рекурсивного вызова make не происходит
	@echo "Generate: .vscode/compile_commands.json"
	@compiledb -n -o .vscode/compile_commands.json make -f builder.mk -j 8

# Цель для создания bin файла из elf/exe файла
$(BIN): $(OUT)
	@echo "Generate: $@"
	@$(OBJCOPY) -O binary $< $@ 

# Цель для создания lss файла из elf/exe файла
$(LSS): $(OUT)
	@echo "Generate: $@"
	@$(OBJDUMP) -h -S $< > $@

# Цель для линковки elf/exe файла из набора всех объектных файлов
$(OUT): $(ALL_OBJ)
	@echo "Linking: $@"
	@$(G++) $(G++_CFLAGS) $(G++_LFLAGS) -o $@ $(ALL_OBJ) $(LIBS)
	@$(SIZE) $@

# Включаем файлы с зависимостями для отслеживания изменений в заголовочных файлах
-include $(DEPENDS)

# Шаблон для компиляции всех C++ файлов
$(OUTPUT_PATH)%.o: %.cpp $(OTHER_DEPS)
	@echo "Compile: $<"
	@$(MKD) $(dir $@)
	@$(G++) $(G++_CFLAGS) -c -o $@ $<

# Шаблон для компиляции всех C файлов
$(OUTPUT_PATH)%.o: %.c $(OTHER_DEPS)
	@echo "Compile: $<"
	@$(MKD) $(dir $@)
	@$(GCC) $(GCC_CFLAGS) -c -o $@ $<

# Шаблон для компиляции всех ASM файлов
$(OUTPUT_PATH)%.o: %.s $(OTHER_DEPS)
	@echo "Compile: $<"
	@$(MKD) $(dir $@)
	@$(GCC) $(GCC_CFLAGS) -c -o $@ $<

